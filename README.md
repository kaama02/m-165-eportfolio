# ePortfolie für Modul-165 (NoSQL-Datenbanken einsetzen)

### [MongoDB Queries/Statements](#mongodb-queries-statements)
- [.find()](#find)
- [.insertOne()](#insertone)
- [.insertMany()](#insertmany)
- [.updateOne()](#updateone)
- [.updateMany()](#updatemany)
- [.deleteOne()](#deleteone)
- [.deleteMany()](#deleteMany)

### [Mapping Conceptual Model Relationship in MongoDB](#mapping)
- [One-to-One](#one-to-one)
- [One-to-Many](#one-to-many)
- [Many-to-Many](#many-to-many)

### [SQL Concepts in MongoDB](#sql-concepts-in-mongodb)
- [Referential Integrity](#referential-integrity)
- [Normalization](#normalization)
- [Primary Key](#primary-key)
- [Foreign Key](#foreign-key)

### [NoSQL Database](#nosql-database)
- [NoSQL Database types](#nosql-database-types)
- [Aufgaben](#aufgaben)

### [ACID](#acid)
-  [Atomicity](#atomicity)
-  [Consistency](#consistency)
-  [Isolation](#isolation)
-  [Durability](#durability)

### [CAP-Theorem](#cap-theorem)
- [Properties](#properties) (Consistency, Availability, Partition tolerance)
- [Implications](#implications) (AC, AP, CP)

### [BASE](#base)
- [Basically Available](#basically-available)
- [Soft State](#soft-state)
- [Eventual consistency](#eventual-consistency)
- [Comparison of ACID and BASE](#comparison-of-acid-and-base)

### [Scaling Forms](#scaling-forms)
- [Replication](#replication)
- [Sharding](#sharding)
- [MongoDB and Scaling](#mongodb-and-scaling) 


## MongoDB Queries/Statements

### .find()
```js
// Find all documents in the 'users' collection
db.users.find({});

// Find documents with specific criteria
db.users.find({ age: { $gte: 18 } });
```

### .insertOne()

```js
// Insert a single document into the 'users' collection
db.users.insertOne({
  name: "John Doe",
  age: 30,
  email: "john.doe@example.com"
});
``` 

### .insertMany()

```js
// Insert multiple documents into the 'users' collection
db.users.insertMany([
  { name: "Alice", age: 25, email: "alice@example.com" },
  { name: "Bob", age: 28, email: "bob@example.com" }
]);
``` 

### .updateOne()

```js
// Update a single document in the 'users' collection
db.users.updateOne(
  { name: "John Doe" },
  { $set: { age: 31 } }
);
``` 

### .updateMany()

```js
// Update multiple documents in the 'users' collection
db.users.updateMany(
  { age: { $lt: 30 } },
  { $set: { status: "young adult" } }
);
``` 

### .deleteOne()

```js
// Delete a single document from the 'users' collection
db.users.deleteOne({ name: "John Doe" });
``` 

### .deleteMany()

```js
// Delete multiple documents from the 'users' collection
db.users.deleteMany({ status: "young adult" });
``` 

## Mapping Conceptual Model Relationship in MongoDB
### One-to-One:
Embedded documents or referencing with ObjectId.
```js
// Embedded document
{
  name: "John Doe",
  address: {
    street: "123 Main St",
    city: "Anytown",
    postalCode: "12345"
  }
}
``` 

### One-to-Many: 
Embedded documents for small related data or referencing for larger related data.
```js
// Referencing
{
  _id: ObjectId("authorId1"),
  name: "Harper Lee",
  books: [ObjectId("bookId1"), ObjectId("bookId2")]
}
```

### Many-to-Many: 
Referencing documents in an array or using a junction collection.
```js
// Junction collection (Loans)
{
  _id: ObjectId("loanId1"),
  borrower: ObjectId("borrowerId1"),
  book: ObjectId("bookId1"),
  loanDate: "2023-03-01",
  returnDate: "2023-03-15"
}
```

## SQL Concepts in MongoDB

### Referential Integrity:
- SQL:     Enforced using foreign keys.
- MongoDB: Managed manually using references (ObjectId). There are no foreign keys, but consistency can be maintained through application logic.

### Normalization:
- SQL:     Data is normalized to reduce redundancy.
- MongoDB: Often prefers denormalization for performance reasons. Embedding documents is common to avoid joins.

### Primary Key:
- SQL:     Unique identifier for records, often auto-incremented.
- MongoDB: _id field serves as the primary key, which is usually a unique ObjectId.

### Foreign Key:
- SQL: A field in one table that uniquely identifies a row in another table.
- MongoDB: Foreign keys are not used, but references to other documents are managed through ObjectId.

## RDBMS
Relational databases use a table-based structure to organize data. They adhere to a strict schema and use SQL for querying and managing data.

***Use Case:*** <br>
- Finance and Banking: Managing transactions and financial records.
- Enterprise Applications: Applications that require complex queries and relationships, such as ERP systems.
- E-commerce: Inventory management, customer data, and order processing.

## NoSQL Database
A NoSQL database provides a mechanism for storage and retrieval of data that is modeled in means other than the tabular relations used in relational databases. Unlike relational databases, NoSQL databases are built to have flexible schemas, scale horizontally, and are designed to handle large volumes of data and high traffic loads. They are particularly effective for applications that require rapid read/write operations with large datasets or databases distributed across geographical locations.

***Use Case:*** <br>
- Real-Time Analytics: Analyzing and processing large volumes of data in real-time.
- SocialMedia Applications: Managing user profiles, posts, comments, and relationships.

### NoSQL Database types:

1. [Document Stores](#document-stores)
2. [Key-Value Stores](#key-value-stores)
3. [Column-Family Stores](#column-family-stores)
4. [Graph Database](#graph-database)

### Document Stores
Document stores, also known as document-oriented databases, store data as documents. Each document is a self-contained unit, typically encoded in formats such as JSON, BSON, or XML. These documents are schema-flexible, allowing for dynamic and hierarchical data storage.

***Examples:*** MongoDB, CouchDB, Amazon DocumentDB <br>

***Use Case:*** <br>
- Content management system
- E-commerce applications
- Real-time analytics
- Applications requiring flexible schemas

**Key components:** <br>
- Document: The basic unit of data storage, which includes fields and values.
- Collection: A grouping of documents, analogous to a table in relational databases.
- Index: Structures that improve the speed of data retrieval operations.

**Pros vs Cons:** <br>
| Pros                                                        | Cons                                                                              |
| ----------------------------------------------------------- | ----------------------------------------------------------------------------------|
| Flexible schema allows for easy modification and iteration. | Complex queries and joins can be less efficient compared to relational databases. |
| Ideal for hierarchical and nested data structures.          | Schema flexibility can lead to inconsistent data structures.                      |
| High performance for read and write operations.             |  |


### Key-Value Stores
Key-value stores are the simplest type of NoSQL databases. They store data as a collection of key-value pairs, where the key is a unique identifier, and the value is the data associated with the key. These databases are highly optimized for quick retrieval of values based on keys.

***Examples:*** Redis, Amazon DynamoDB, Riak <br>

***Use Case:*** <br>
- Caching
- Session management
- Real-time data analytics
- Simple configuration storage


**Key components:** <br>
- key
- value

**Pros vs Cons:** <br>
| Pros                             | Cons                          |
| -------------------------------- | ------------------------------|
| Extremely fast for read and write operations. | Limited querying capabilities; primarily key-based access. |
| Simple data model, easy to understand and implement. | Not suitable for complex queries or relationships between data. |
| Highly scalable and distributed. |  |

### Column-Family Stores
Column-family stores, also known as wide-column stores, organize data into columns and column families. Each column family contains rows, where each row can have a different number of columns. This model is highly efficient for read and write operations over large datasets.

***Examples:*** Apache Cassandra, HBase, ScyllaDB <br>

***Use Case:*** <br>
- Time-series data
- IoT applications
- Real-time analytics
- High-velocity data ingestion

**Key components:** <br>
- Column: Highly scalable and distributed, suitable for large datasets.
- Column Family: A container for a set of columns that are logically related.
- Row: A collection of columns within a column family, identified by a unique row key.

**Pros vs Cons:** <br>
| Pros                             | Cons                          |
| -------------------------------- | ------------------------------|
| Highly scalable and distributed, suitable for large datasets. | Complex data model can be challenging to design and maintain. |
| Efficient read and write operations for large-scale data. | Not ideal for applications requiring ACID transactions. |
| Flexible schema allows for varying column numbers per row. | Limited support for ad-hoc queries and joins. |


### Graph Database
Graph-based databases are designed for data whose relations are well represented as a graph and has elements that are interconnected, with an undetermined number of relations between them. They are ideal for handling data like social relationships, networks, and hierarchies. <br>

***Examples:*** Neo4j, ArangoDB, OrientDB <br>

***Use Case:*** <br>
- Recommendation engines
- Social Networks

**Key components:** <br>
- Nodes: represents entities or objects.
- Edges: lines that connects the nodes, represents the relationship between them.
- Properties: information attached to nodes and edges
- Labels

**Pros vs Cons:** <br>
| Pros                             | Cons                          |
| -------------------------------- | ------------------------------|
| Perfomance:   Relationships are stored at the individual record level, allowing high-performance retrieval and query of connected data | Scalibily: they are designed for one-tier architecture meaning that they are hard to be scaled across a number of servers. |
| Flexibility: The schema-free nature of graph databases allows developers to add new kinds of relationships, nodes, and properties without disturbing existing data. | Lack of uniform query language |
| Agility: Linked data with indexed connections allows for more agile data discovery and analytics. | Write Speeds: Graph databases can sometimes exhibit slower write speeds compared to other databases due to the overhead of maintaining relationships (edges) between nodes. |

## ACID
The abbreviation ACID ensures that transactions are processed reliably and that the database remains in a consistent state even in the presence of errors, power failures, or other problems.

### Atomicity
Atomicity ensures that all operations within a transaction are completed successfully or none at all. This means that a transaction cannot be partially completed; it is either fully executed or fully rolled back.

### Consistency
Consistency ensures that a transaction brings the database from one valid state to another. Any data written to the database must be valid according to all defined rules, including constraints, cascades, and triggers.

### Isolation
Isolation ensures that concurrent transactions do not interfere with each other. Each transaction must execute in isolation from others, preventing issues such as dirty reads, non-repeatable reads, and phantom reads.

### Durability
Durability ensures that once a transaction has been committed, it remains in the system even in the event of a system failure. This means that committed transactions are saved permanently.

## CAP-Theorem
The CAP Theorem, also known as Brewer's Theorem, states that in a distributed data system, it is impossible to simultaneously achieve all three of the following properties: Consistency, Availability, and Partition Tolerance. Due to the inherent limitations and trade-offs in distributed systems, only two of these three properties can be fully satisfied at any given time.

### Properties
1. Consistency **C** <br>
The term consistency describes the fact that each node contains the same content at all times or returns the same content when a request is made. The nodes are therefore constantly synchronised.
This consistency must not be confused with the consistency of the ACID, which only concerns the internal consistency of a data set.

2. Availability **A** <br>
Every request (read or write) receives a response, the point here is not that every node is constantly up and running. This ensures that the system remains operational and responsive. The system is not viewed as a whole, as is the case with a web application, for example, where it is sufficient for the load balancer to return a response.

3. Partition Tolerance **P** <br>
The system continues to operate despite arbitrary partitioning due to network failures. This means the system can handle loss of network connectivity between nodes. A system is considered "partitioned" if two nodes cannot communicate with each other.

### Implications
1. Consistency and Availability **AC** <br>
   - Trade-off: Partition Tolerance is sacrificed.
   - Implication: Suitable for systems where network partitions are rare, such as within a single data center with a reliable network. The system can afford to become unavailable during partitions to maintain consistency.
   - Example: Relational databases (e.g., traditional RDBMS like MySQL in a single data center).

2. Consistency and Partition Tolerance **CP** <br>
   - Trade-off: Availability is sacrificed.
   - Implication: Suitable for systems where consistency is crucial, and the system can afford to become unavailable in the event of a network partition to maintain consistency.
   - Example: Distributed databases requiring strong consistency (e.g., HBase, MongoDB in strict consistency mode).

3. Availability and Partition Tolerance **AP** <br>
   - Trade-off: Consistency is sacrificed.
   - Implication: Suitable for systems where availability is critical, and the system can accept eventual consistency. The system continues to operate even during network partitions, providing the best effort response.
   - Example: NoSQL databases like Cassandra, DynamoDB which favor availability and partition tolerance over immediate consistency.


## BASE
It is a model used to provide a more flexible approach to database management, particularly for NoSQL databases. BASE is often seen as the opposite of the ACID properties found in traditional relational databases. It is designed to handle the challenges of distributed systems and large-scale applications by relaxing some of the constraints that ACID enforces.

### Basically Available 
The system guarantees availability of data, meaning that requests will receive a response, but not necessarily the most recent data.

### Soft State
The state of the system may change over time, even without input. This contrasts with the ACID property of consistency, where the state is always consistent.

### Eventual Consistency
The system will eventually become consistent once all updates have propagated through the system. There is no guarantee of immediate consistency.

### Comparison of ACID and BASE
| ACID                                                        | BASE                                                                              |
| ----------------------------------------------------------- | ----------------------------------------------------------------------------------|
| Guarantees immediate consistency. | Provides eventual consistency, which may allow temporary inconsistencies. |
| Can compromise availability to ensure consistency and durability. | Prioritizes availability, even if it means sacrificing immediate consistency. |
| Suitable for applications where data integrity and consistency are paramount (e.g., financial systems, inventory management). | Suitable for large-scale, distributed applications where availability and performance are prioritized over immediate consistency (e.g., social networks, real-time analytics). |

## Scaling Forms
### Replication
Replication involves copying data across multiple servers to ensure data redundancy and increase availability. It helps in providing fault tolerance and enables data to be available even if some of the servers fail.

***Example:*** Master-Slave Replication: <br> In this model, one server acts as the master and handles all write operations, while multiple slave servers replicate the master's data and handle read operations. If the master fails, one of the slaves can be promoted to master. <br>

***Use Case:*** Online applications that require high availability and read scalability, such as social media platforms. <br>

### Sharding
Sharding, also known as horizontal partitioning, involves distributing data across multiple servers or nodes based on a shard key. Each shard contains a subset of the total data, and together, all shards form the complete dataset.

***Example:*** Hash-based Sharding: <br> Data is distributed across shards using a hash function on the shard key. This ensures an even distribution of data. <br>

***Use Case:*** Applications with large datasets that need to be distributed to manage load and ensure performance, such as large-scale e-commerce platforms. <br>

### MongoDB and Scaling
- **Replication in MongoDB** <br>
Components: 
    - Primary (Master): The node that receives all write operations. There is only one primary in a replica set.
    - Secondary (Slave): Nodes that replicate data from the primary. They can handle read operations and provide data redundancy.
    - Arbiter: A node that participates in the election process for a new primary but does not hold data. It helps in maintaining an odd number of votes to avoid ties during elections. <br> 

- Write Operations: Only occur on the primary.
- Read Operations: Can occur on both primary and secondary nodes, depending on the read preference configuration.
- Failover: If the primary fails, an election process takes place among the secondaries to choose a new primary.

In a MongoDB replica set with one primary and two secondaries, if the primary goes down, an election occurs, and one of the secondaries is promoted to primary, ensuring continuous availability.

- **Sharding in MongoDB** <br>
Components: 
    - Shard: Each shard is a replica set or a single server that holds a subset of the data.
    - Config Server: Stores metadata and the configuration settings for the sharded cluster. There are typically three config servers for redundancy.
    - Mongos (Router): Acts as a query router. It directs client requests to the appropriate shard(s) based on the shard key and the metadata stored in the config servers. 

- Shard Key: A key chosen to distribute the data. It determines the distribution of data across shards.
- Data Distribution: Data is split into chunks and distributed across shards based on the shard key. Each chunk contains a range of data.
- Query Routing: Mongos uses the shard key and config server metadata to route queries to the correct shards.

In a sharded MongoDB cluster with three shards, data about user transactions might be distributed based on a hashed user ID as the shard key. Queries about a specific user's transactions would be routed by Mongos to the shard holding the relevant data, ensuring efficient data access and load distribution.


### Aufgaben
- [KN01 ](./KN01/)
- [KN02 ](./KN02/)
- [KN03 ](./KN03/)
- [KN04 ](./KN04/)
- [KN05 ](./KN05/)
- [KN06 ](./KN06/)
- [KN07 ](./KN07/)
- [KN08 ](./KN08/)
- [KN09 ](./KN09/)