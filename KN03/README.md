# KN03 - Datenmanipulation und Abfragen I

### A) Daten hinzufügen (25%)
[daten_hinzufuegen.json](./res/daten_hinzufuegen.js)

### B) Daten löschen (25%)
[daten_loeschen.json](./res/daten_loeschen.js) | [daten_löschen_teilweise.json](./res/daten_loeschen_teilweise.js)

### C) Daten abfragen (25%)
[daten_abfragen.json](./res/daten_abfragen.js)

### D) Daten verändern (25%)
[daten_veraendern.json](./res/daten_veraendern.js)
