db.person.find().pretty()

db.workgroup.find({ desc: { $exists: true } }).pretty()

db.person.find({
    joinedAt: {
        $gte: ISODate("2023-01-01T00:00:00Z"),
        $lt: ISODate("2023-03-01T00:00:00Z")
    }
}).pretty()

db.workgroup.find({
    $or: [
        { name: "Rikshung" },
        { members: { $exists: false, $size: 0 } }
    ]
}).pretty()

db.person.find({
    $and: [
        { SectionID: ObjectId("668265c1564efa20dabba7de") },
        { joinedAt: ISODate("2023-05-10T00:00:00Z") }
    ]
}).pretty()


db.country.find({
    name: { $regex: /Switzerland/, $options: 'i' }
}).pretty()

db.person.find({}, {
    _id: 1,
    name: 1,
    email: 1
}).pretty()

db.workgroup.find({}, {
    _id: 0,
    name: 1,
    desc: 1
}).pretty()