db.person.updateOne(
  { name: "Gewa Dorjee" },
  { $set: { phone_number: 123123123 } }
);

db.workgroup.updateMany(
  { $or: [{ name: "Rikshung" }, { name: "Yak Nation" }] },
  { $set: { desc: "Updated description for selected workgroups." } }
);

db.section.replaceOne(
  { name: "Smart Watch" },
  {
    name: "Apple Smart Watch",
    price: 249.5,
    description: "Wearable Computer on your wrist from Apple",
  }
);

db.section.replaceOne(
    { name: "Choelsum" },
    {
        _id: ObjectId("66826144564efa20dabba7c0"),
        name: "Choelsum Updated",
        members: [
            ObjectId("66826144564efa20dabba7bc"), 
            ObjectId("66826144564efa20dabba7be") 
        ]
    }
);

