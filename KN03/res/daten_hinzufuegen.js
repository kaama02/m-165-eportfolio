const workGroup1 = new ObjectId();
const workGroup2 = new ObjectId();
const workGroup3 = new ObjectId();

const person1Id = new ObjectId();
const person2Id = new ObjectId();
const person3Id = new ObjectId();
const person4Id = new ObjectId();

const section1Id = new ObjectId();
const section2Id = new ObjectId();

const country1Id = new ObjectId();

db.workgroup.insertMany([
    { 
        _id: workGroup1, 
        name: 'Rikshung', 
        desc: 'promoting and preserving Tibetan culture.', 
        Members: [person3Id, person4Id] 
    },
    {
        _id: workGroup2, 
        name: 'Shenpen', 
        desc: 'Helping Tibetan Refugees', 
        Members: [person1Id, person4Id] 
    },
    { 
        _id: workGroup3, 
        name: 'Yak Nation', 
        desc: 'In order to appeal to the younger upcoming generation, especially the Tibetan youth, ‘Yak Nation Studios’ as a VTJE brand offers ideal conditions for its own identification.', 
        Members: [person2Id, person3Id] 
    }
]);

db.person.insertMany([
    {
        _id: person1Id,
        name: 'Choenyi Sangmo',
        email: 'choenyi.sangmo@example.com',
        phone_number: '987654321',
        Street: '456 Oak St',
        Postcode: '67890',
        City: 'Gotham',
        joinedAt: new Date('2021-01-01'),
        SectionID: section1Id,
    },
    {
        _id: person2Id,
        name: 'Tashi Yangzom',
        email: 'tashi.yangzom@example.com',
        phone_number: '111222333',
        Street: '789 Pine St',
        Postcode: '11223',
        City: 'Star City',
        joinedAt: new Date('2021-02-15'),
        SectionID: section2Id,
    },
    {
        _id: person3Id,
        name: 'Pema Choetso',
        email: 'pema.choetso@example.com',
        phone_number: '444555666',
        Street: '101 Maple St',
        Postcode: '33445',
        City: 'Central City',
        joinedAt: new Date('2021-03-10'),
        SectionID: section1Id,
    },
    {
        _id: person4Id,
        name: 'Gewa Dorjee',
        email: 'gewa.dorjee@example.com',
        phone_number: '123456789',
        Street: '123 Elm St',
        Postcode: '12345',
        City: 'Zürich',
        joinedAt: new Date('2021-04-20'),
        SectionID: section2Id,
    }
]);

db.country.insertOne({
    _id: country1Id,
    name: 'Switzerland',
    sections: [
        {
            _id: section1Id,
            name: 'Choelsum',
            founded: new Date('2020-01-02')
        },
        {
            _id: section2Id,
            name: 'Tsethang',
            founded: new Date('2021-01-04')
        }
    ]
});

