# KN08: Datenabfrage und -Manipulation

### A) Daten hinzufügen
- skrip-Datei (.txt)

### B) Daten abfragen
- Erklärung des Statements, um alle Knoten und Kanten zu lesen, speziell die OPTIONAL
Klausel:

- Beschreibungen der 4 Szenarien und Cypher-Statements dazu:

### C) Daten löschen
- Ihre Statements - einmal ohne und einmal mit DETACH.

- Vorher-Nachher Screenshots und Erklärungen dazu.

### D) Daten verändern
- Beschreibungen der 3 Szenarien und Cypher-Statements dazu:

### E) Zusätzliche Klauseln
- Beschreibungen, Erklärungen zum Anwendungsfall (das Beispiel) und Cypher-Statement dazu:
