# KN05: Administration von MongoDB

### A) Rechte und Rollen
1. Screenshot des Fehlers bei einer Verbindung mit der falschen Authentifizierungsquelle:
![](./images/1.png)

2. Skript, welches die beiden Benutzer erstellt:
    ```ruby
    use tyae;
    db.createUser({
    user: "readOnlyUser",
    pwd: "password1",
    roles: [{ role: "read", db: "tyae" }]
    });

    use admin;
    db.createUser({
    user: "readWriteUser",
    pwd: "password2",
    roles: [{ role: "readWrite", db: "tyae" }]
    });

    ```
    User 1: <br>
    ![](./images/2.png)

    User 2: <br>
    ![](./images/3.png)

3. Screenshots, die zeigen, dass die Rechte für Benutzer 1 funktionieren, im Speziellen:

    - Screenshot für das Einloggen (Verbindungstext sichtbar): <br>
      ![](./images/4.png)

    - Screenshot für das Lesen von Daten ohne Fehler: <br>
      ![](./images/5.png)

    - Screenshot für das Schreiben von Daten mit Fehler: <br>
      ![](./images/6.png)

4. Screenshots, die zeigen, dass die Rechte für Benutzer 2 funktionieren, im Speziellen:

    - Screenshot für das Einloggen (Verbindungstext sichtbar): <br>
      ![](./images/7.png)

    - Screenshot für das Lesen von Daten ohne Fehler: <br>
      ![](./images/8.png)

    - Screenshot für das Schreiben von Daten ohne Fehler. <br>
      ![](./images/9.png)

### B) Backup und Restore

1. Backup Variante 1:
    - Screenshots des jeweiligen Status. Es muss ersichtlich sein, dass Daten weg sind und anschliessend wieder verfügbar: <br>
      ```sudo mongodump --uri="mongodb://admin:capybara@3.94.32.14:27017/?authSource=admin&readPreference=primary&ssl=false&directConnection=true" --db=tyae --out "/home/ubuntu/dump"```
      ![](./images/10.png)

      Database "tyae" gelöscht: <br>
      ![](./images/11.png)
      ```sudo mongorestore --uri="mongodb://admin:capybara@3.94.32.14:27017/?authSource=admin&readPreference=primary&ssl=false&directConnection=true"  "./dump"```
      ![](./images/12.png)
      ![](./images/16.png)

2. Backup Variante 2: 
    - Screenshots des jeweiligen Status. Es muss ersichtlich sein, dass Daten weg sind und anschliessend wieder verfügbar. <br>
      ![](./images/13.png) <br>
      ![](./images/15.png) <br>
      ![](./images/14.png) <br>

### C) Skalierung
## Scaling Forms
Replication and partitioning are two fundamental techniques in database management that improve availability, scalability, and fault tolerance. 

### Replication
Replication involves creating and managing exact copies of data across multiple servers. These copies are called replicas.

***Use Case:*** Online applications that require high availability and read scalability, such as social media platforms. <br>

### Sharding
Partitioning, or sharding, involves dividing a large database into smaller, more manageable pieces, which are stored across multiple servers.

***Use Case:*** Applications with large datasets that need to be distributed to manage load and ensure performance, such as large-scale e-commerce platforms. <br>

![](./images/17.png)
