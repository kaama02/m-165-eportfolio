# KN07: Installation und Datenmodellierung für Neo4j

### A) Installation / Account erstellen
*Created AWS instance and verified Neo4j installation:* 
<br> 
![](./images/3.png)
![](./images/1.png)

*Cloud Hosting, Account created in the Cloud:* 
<br>
![](./images/2.png)

## B) Logisches Modell für Neo4j
![](./images/4.png)
