# KN02 Datenmodellierung für MongoDB

### A) Konzeptionelles Datenmodell

- Draw.io Diagramm des konzeptionellen Diagramms: <br>
[Konzeptionelles Modell Draw.io Datei](./M-165_kn02_Konzeptionelles-Modell.drawio)

- Bild des konzeptionellen Diagramms: <br>
![](./images/1.png)

- I am a member of TYAE (Tibetan Youth Association Europe). It is a big network for tibetan youth across europe interested in getting politically involved. So multiple tibetans in european countries participates. <br> 
Each **country** has multiple sections, a **section** can only belong to one country (**1:m**). <br>
A **section** has multiple members or none inside and a **member** can belong to only one section (**1:mc**). <br>
A **workgroup** has multiple member and every **person** is in multiple workgroup. (**m:m**)


### B) Logisches Modell für MongoDB

- Ein Bild des logischen Datenmodells. <br>
![](./images/2.png)

- Die Original-Datei des logischen Datenmodells (z.B. draw.io). <br>
[Logisches Modell Draw.io Datei](./M-165_kn02_Logisches-Modell.drawio)

- Erklärung zu Verschachtelungen. wieso haben Sie Ihre Variante gewählt: <br>
As *Person* and *Workgroup* has many to many relation, they are separately visualized. <br>
*Section* is nested inside *country* as a country has multiple section but a section belongs to one country. <br>
*Person* is connected to the *Section* box because, each person belongs to one section. And a section has a multiple members.


### C) Anwendung des Schemas in MongoDB

- Script mit den Befehlen zur Erstellung der Collections.

```
use tyae

db.createCollection("person")
db.createCollection("country")
db.createCollection("workgroup")
```

![](./images/3.png)

