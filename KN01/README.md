# KN01: Installation und Verwaltung von MongoDB

### A) Installion 

- [Cloud-init Datei](./cloudinit-mongodb.yaml) mit dem geänderten Password

- Screenshot von Compass mit der Liste der bereits bestehenden Datenbanken: <br>
![](./images/1.png)

- `mongodb://admin:capybara@3.94.32.14:27017/?authSource=admin&readPreference=primary&ssl=false&directConnection=true` <br>
   The authSource parameter in the MongoDB connection string specifies which database MongoDB should use for authentication
   purposes. This setting is particularly appropriate and secure for setups where administrative credentials need to be centralized and where
   users might need access across multiple databases.

- `sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mongod.conf` <br>
   It replaces the IP address 127.0.0.1 (the loopback address, limiting connections to the local machine) with 0.0.0.0 (MongoDB to listen on
   all network interfaces), allowing it to accept connections from any remote address (unless restricted by other means such as firewalls).

  `sudo sed -i 's/#security:/security:\n  authorization: enabled/g' /etc/mongod.conf`  <br>
   It finds the line `#security:` and replaces it with security: followed by a new line adding `authorization: enabled`. This change uncomments the security settings and activates user authentication, requiring valid user credentials for access to the database.
  
  ![](./images/2.png)

### B) Erste Schritte GUI
- [JSON file/document](./tsering.json) 
- Screenshot Ihrer Compass-Applikation mit der Datenbank, Collection und Dokument sichtbar. <br> ![](./images/3.png)
- Export-Datei <br>
  [exported-anodunkhartsang.tsering.json](./exported-anodunkhartsang.tsering.json) <br>
  birthdate was saved as string before, to set the birthdate datatype as date, the json file should specify it as:

  ```
  {
    "firstName": "Tsering Lhamo",
    "lastName": "Anodunkhartsang",
    "address": "Arbergstrasse 7a, 8405 Winterthur",
    "height": 158,
    "birthdate": {
      "$date": "2002-11-13T00:00:00.000Z"
    }
  }

  ```
  ![](./images/6.png)

### C) Erste Schritte Shell 

- Befehl liste. Was machen die Befehle 1-5? <br>
![](./images/4.png)<br>
1. `show dbs`, `show databases` : lists all the databases present in the MongoDB server.
2. `use anodunkhartsang` : switches (or creates if it doesn't exist) the current database to the specified one.
3. `show collections`, `show tables` : list all the collections in the current database.

- Screenshot von der MongoDB-Shell auf dem Linux-Server, der zeigt, dass Sie die Befehle
eingegeben haben.
![](./images/5.png)<br>